# Compas Online Test Backend - Satri Nugraha

## Solution Schema
<img src="https://gitlab.com/compas6/online-test/-/raw/master/Solution-Schema.png">

## Technology
<ul>
    <li><b>Application Gateway</b> yang digunakan berfungsi sebagai load balancer atau pembagi beban baik akses internal ataupun eksternal.</li>
    <li><b>WAF (Web Application Firewall)</b> yang digunakan untuk memfilter dan memonitoring traffic atau request dari client.</li>
    <li><b>API Manager</b> berfungsi untuk memanage traffic, manajemen otentikasi dan pengguna serta analisa API</li>
    <li><b>Workflow Engine</b> berfungsi untuk memisahkan logic dengan fungsi aplikasi.</li>
    <li><b>Docker</b> untuk deployment aplikasi dalam bentuk virtual container.</li>
    <li><b>Flat File</b> dibuat untuk setiap request untuk menghindari loss transaction ke database</li>
    <li><b>ETL</b> digunakan untuk mentransformasi flat file ke dalam database</li>
    <li><b>CI/CD</b> digunakan untuk mendeploy aplikasi dari repository</li>
</ul>

## API Documentation
<a href="https://documenter.getpostman.com/view/9817772/2s8Yt1r9Z9">https://documenter.getpostman.com/view/9817772/2s8Yt1r9Z9</a>