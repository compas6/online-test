/**
 * @author Satri Nugraha
**/
package id.co.compas.onlinetest.controller;

import id.co.compas.onlinetest.mapper.PointUserMapper;
import id.co.compas.onlinetest.services.PointUserInterface;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PointUser {
    @Autowired
    PointUserInterface main;
    
    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<Map<String, Object>> getdefault(){
        Map<String, Object> map = new HashMap<>();
        map.put("code", 400);
        map.put("message", "Invalid request");        
        return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/createPoint", method = {RequestMethod.POST})
    public ResponseEntity<Map<String, Object>> createPoint(@RequestHeader("api-key") String apiKey, @RequestBody PointUserMapper data){
        Map<String, Object> map = new HashMap<>();
        if(!main.simpleAuth(apiKey)){
            map.put("code", 401);
            map.put("message", "Unauthorized");        
            return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
        }
        map = main.createPoint(data);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/addPoint", method = {RequestMethod.POST})
    public ResponseEntity<Map<String, Object>> addPoint(@RequestHeader("api-key") String apiKey, @RequestBody PointUserMapper data){
        Map<String, Object> map = new HashMap<>();
        if(!main.simpleAuth(apiKey)){
            map.put("code", 401);
            map.put("message", "Unauthorized");        
            return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
        }
        map = main.addPoint(data);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/minPoint", method = {RequestMethod.POST})
    public ResponseEntity<Map<String, Object>> minPoint(@RequestHeader("api-key") String apiKey, @RequestBody PointUserMapper data){
        Map<String, Object> map = new HashMap<>();
        if(!main.simpleAuth(apiKey)){
            map.put("code", 401);
            map.put("message", "Unauthorized");        
            return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
        }
        map = main.minPoint(data);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
    
}
