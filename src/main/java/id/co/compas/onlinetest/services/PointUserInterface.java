/**
 * @author Satri Nugraha
**/
package id.co.compas.onlinetest.services;

import id.co.compas.onlinetest.mapper.PointUserMapper;
import java.util.HashMap;

public interface PointUserInterface {
    boolean simpleAuth(String key);
    HashMap<String, Object> createPoint(PointUserMapper data);
    HashMap<String, Object> addPoint(PointUserMapper data);
    HashMap<String, Object> minPoint(PointUserMapper data);
}
