/**
 * @author Satri Nugraha
**/
package id.co.compas.onlinetest.model;

import id.co.compas.onlinetest.mapper.PointUserMapper;
import id.co.compas.onlinetest.services.PointUserInterface;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class PointUserModel implements PointUserInterface{
@Autowired
    private JdbcTemplate jdbcTemplate;
    private final String defaultKey = "78c47c507ba33e1af0db5966394af04190479a00feffdf41c4226a84f26f28bb";
    
    @Override
    public boolean simpleAuth(String key) {
        return key.equals(defaultKey);
    }

    @Override
    public HashMap<String, Object> createPoint(PointUserMapper data) {
        Map<String, Object> map = new HashMap<>();
        try{
            jdbcTemplate.update("insert into user_point(user, point, created_at) values(?,?,now())",data.getUser(),data.getPoint());            
            map.put("code", 200);
            map.put("message", "success");                    
        }catch(DataAccessException e){
            map.put("code", 500);
            map.put("message", e.getMessage());                    
        }
        return (HashMap<String, Object>) map;
    }

    @Override
    public HashMap<String, Object> addPoint(PointUserMapper data) {
        Map<String, Object> map = new HashMap<>();
        try{
            jdbcTemplate.update("update user_point set point=point+?, last_updated=now() where id=?",data.getAdd(),data.getId());            
            map.put("code", 200);
            map.put("message", "success");                    
        }catch(DataAccessException e){
            map.put("code", 500);
            map.put("message", e.getMessage());                    
        }
        return (HashMap<String, Object>) map;
    }

    @Override
    public HashMap<String, Object> minPoint(PointUserMapper data) {
        Map<String, Object> map = new HashMap<>();
        try{
            jdbcTemplate.update("update user_point set point=point-?, last_updated=now() where id=?",data.getMinus(),data.getId());            
            map.put("code", 200);
            map.put("message", "success");                    
        }catch(DataAccessException e){
            map.put("code", 500);
            map.put("message", e.getMessage());                    
        }
        return (HashMap<String, Object>) map;
    }
    
}
